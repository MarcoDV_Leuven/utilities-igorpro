#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Function SplitDataByStagePositionHACKED(toDisk, folderPath, extension)
	variable toDisk
	string folderPath, extension
	string listOfPaths = FindFiles(folderPath, extension, recurse=0)
	variable numberOfFiles = itemsInList(listOfPaths)
	variable fileCounter
	
	DFREF packageFolder = root:Packages:TimeScreening
	
	for (fileCounter=0; fileCounter < numberOfFiles; fileCounter ++)
		string currentPath = stringfromList(fileCounter, listOfPaths)
		variable imsID = IMSOpenFile(currentPath)
		
		string outputFolderName_dirty = stringfromList(1, currentPath, folderPath+":")
		string outputFolderPath = folderPath + ":" + replaceString(".tif", outputFolderName_dirty, "")
		print "Processing file: " + outputFolderName_dirty
		
		NewPath/C tempPath, outputFolderPath // creates folder in local disk
		KillPath tempPath
		
		wave /Z/wave W_ProgramRoot
		wave /Z/T W_AcqNames
		string imagerProgram = LoadImagerProgram(IMSGetImagerProgram(imsID))
		[W_ProgramRoot, W_AcqNames] = ImagerProgramToProgramWaveAndDets(imagerProgram)
		string storageLocation = IMSGetStorageLocation(imsID)
		string measurementName = CleanupName(ParseFilePath(3, storageLocation, "\\", 0, 1), 0)
		wave /T W_DetectorNames = IMSGetDetectorNames(imsID)
		
		NewDataFolder /O $("root:" + measurementName + "_split")
		DFREF outputDF = $("root:" + measurementName + "_split")
		String /G outputDF:S_ImagerProgram = imagerProgram
		
		string outputDiskFolder
		if (toDisk)
	
			outputDiskFolder = outputFolderPath + ":"
			
			
			WriteImagerProgramToDisk(imagerProgram, outputDiskFolder + "ImagerProgram.txt")
		endif
		
		wave /wave W_Result = DetectionsAtNamedPositions(W_ProgramRoot)
		wave /T W_PosNames = W_Result[0]
		wave /wave W_PosDetIndices = W_Result[1]
		wave M_PosCoordinates = W_Result[2]
		
		wave M_DetectionIndices = ImageIdxsForDetectionsInProgram(W_ProgramRoot)
		if (NumPnts(W_AcqNames) != DimSize(M_DetectionIndices, 1))
			Abort "Different number of currently defined acquisitions and those used in program"
		endif
		
		variable nStagePositions = DimSize(W_PosNames, 0)
		variable nAcqTypes = DimSize(M_DetectionIndices, 1)
		variable nDetectorNames = DimSize(W_DetectorNames, 0)
		
		// determine how many images at each position for each type
		Make /FREE/I/U /N=(nStagePositions, nAcqTypes, nDetectorNames) M_nImagesPerAcqType = 0
		variable typeIndex, posIndex, detIndex, imIndex, detNameIndex
		for (typeIndex = 0; typeIndex < nAcqTypes; typeIndex += 1)
			for (posIndex = 0; posIndex < nStagePositions; posIndex+=1)
				wave W_ThisStagePos = W_PosDetIndices[posIndex]
				for (detIndex = 0; detIndex < NumPnts(W_ThisStagePos); detIndex += 1)
					variable thisDetIdx = W_ThisStagePos[detIndex]
					if (NumType(M_DetectionIndices[thisDetIdx][typeIndex]) == 0)
						for (detNameIndex = 0; detNameIndex < nDetectorNames; detNameIndex += 1)
							if (IMSGetNumberOfStoredImages(imsID, W_AcqNames[typeIndex], W_DetectorNames[detNameIndex]) > M_DetectionIndices[thisDetIdx][typeIndex])
								M_nImagesPerAcqType[posIndex][typeIndex][detNameIndex] += 1
							endif
						endfor
					endif
				endfor
			endfor
		endfor
		
		// create all needed output waves
		Make /FREE/WAVE/N=(nStagePositions, nAcqTypes, nDetectorNames) M_OutputWaves
		Make /FREE/WAVE/N=(nStagePositions, nAcqTypes, nDetectorNames) M_OutputWaves_Time
		Make /FREE/WAVE/N=(nStagePositions) M_OutputWaves_Coordinates
		Make /FREE/T/N=(nStagePositions, nAcqTypes, nDetectorNames) M_OutputWaves_BaseOutputNames
		for (posIndex = 0; posIndex < nStagePositions; posIndex += 1)
			string thisPosName = W_PosNames[posIndex]
			NewDataFolder /O outputDF:$thisPosName
			DFREF thisPosDF = outputDF:$thisPosName
			for (typeIndex = 0; typeIndex < nAcqTypes; typeIndex += 1)
				string thisTypeName = W_AcqNames[typeIndex]
				Make /O/N=(3)/D thisPosDF:W_StageCoordinates
				M_OutputWaves_Coordinates[posIndex] = thisPosDF:W_StageCoordinates
				
				MatrixOP /FREE W_thisAcqHasMultipleDetectors = sum(greater(beam(M_nImagesPerAcqType, posIndex, typeIndex), 0)) - 1
				for (detNameIndex = 0; detNameIndex < nDetectorNames; detNameIndex += 1)
					if (M_nImagesPerAcqType[posIndex][typeIndex][detNameIndex] == 0)
						continue
					endif
					string baseOutputName = thisTypeName
					if (W_thisAcqHasMultipleDetectors[0])
						baseOutputName += "_" + W_DetectorNames[detNameIndex]
					endif
					M_OutputWaves_BaseOutputNames[posIndex][typeIndex][detNameIndex] = baseOutputName
					wave M_FirstImage = IMSGetImage(imsID, thisTypeName, W_DetectorNames[detNameIndex], 0)
					Make /O/N=(DimSize(M_FirstImage, 0), DimSize(M_FirstImage, 1), M_nImagesPerAcqType[posIndex][typeIndex][detNameIndex]) /Y=(WaveType(M_FirstImage)) thisPosDF:$baseOutputName
					M_OutputWaves[posIndex][typeIndex][detNameIndex] = thisPosDF:$baseOutputName
					Make /O/N=(M_nImagesPerAcqType[posIndex][typeIndex][detNameIndex]) /D thisPosDF:$(baseOutputName + "_time")
					M_OutputWaves_Time[posIndex][typeIndex][detNameIndex] = thisPosDF:$(baseOutputName + "_time")
				endfor
			endfor
		endfor
		
		// fill the output waves
		variable offset
		for (posIndex = 0; posIndex < nStagePositions; posIndex+=1)
			thisPosName = W_PosNames[posIndex]
			wave M_StageCoordinates = M_OutputWaves_Coordinates[posIndex]
			M_StageCoordinates = M_PosCoordinates[posIndex][p]
			
			if (toDisk)
				string outputDirectory = outputDiskFolder + thisPosName
				NewPath /Q/O/C tempPath, outputDirectory
				if (V_flag != 0)
					Abort "Error creating folder " + outputDirectory
				endif
				KillPath tempPath
				
				string posCoordinatesPath = outputDirectory + ":" + "StageCoordinates.txt"
				Save /G/O M_StageCoordinates as posCoordinatesPath
			endif
			
			for (typeIndex = 0; typeIndex < nAcqTypes; typeIndex += 1)
				thisTypeName = W_AcqNames[typeIndex]
				for (detNameIndex = 0; detNameIndex < nDetectorNames; detNameIndex += 1)
					wave /Z M_Data = M_OutputWaves[posIndex][typeIndex][detNameIndex]
					wave /Z W_TimePoints = M_OutputWaves_Time[posIndex][typeIndex][detNameIndex]
					if (M_nImagesPerAcqType[posIndex][typeIndex][detNameIndex] == 0)
						continue
					endif
					
					if (toDisk)
						string thisFilePath = outputDirectory + ":" + M_OutputWaves_BaseOutputNames[posIndex][typeIndex][detNameIndex] + ".tif"
						variable storerID = IMSNewBasicStorage(thisFilePath)
					endif
					
					offset = 0
					wave W_ThisStagePos = W_PosDetIndices[posIndex]
					for (detIndex = 0; detIndex < NumPnts(W_ThisStagePos); detIndex += 1)
						thisDetIdx = W_ThisStagePos[detIndex]
						if ((NumType(M_DetectionIndices[thisDetIdx][typeIndex]) == 0) && (IMSGetNumberOfStoredImages(imsID, thisTypeName, W_DetectorNames[detNameIndex]) > M_DetectionIndices[thisDetIdx][typeIndex]))
							W_TimePoints[offset] = IMSGetTimePoint(imsID, thisTypeName, W_DetectorNames[detNameIndex], M_DetectionIndices[thisDetIdx][typeIndex])
							
							wave thisImage = IMSGetImage(imsID, thisTypeName, W_DetectorNames[detNameIndex], M_DetectionIndices[thisDetIdx][typeIndex])
							if (!toDisk)
								ImageTransform /P=(offset) /D=thisImage setPlane, M_Data
							else
								IMSAddNewBasicImage(storerID, thisImage)
							endif
							
							offset += 1
						endif
					endfor
					
					if (toDisk)
						IMSCloseBasicStorage(storerID)
						string timePointsPath = outputDirectory + ":" + M_OutputWaves_BaseOutputNames[posIndex][typeIndex][detNameIndex] + "_time.txt"
						Save /G/O W_TimePoints as timePointsPath
					endif
					
				endfor
			endfor
		endfor
		
		if (!toDisk)
			KillWaves /Z M_ImagePlane
		else
			KillDataFolder outputDF
		endif
	endfor
End


// Given a path to a folder on disk, gets all files ending in "ext"
Function/S findFiles(path, ext[, recurse])
// By default, subfolders are searched. Turn off with recurse=0.

    string path, ext; variable recurse
   
    if (paramIsDefault(recurse))
        recurse=1
    endif
    path = sanitizeFilePath(path)                                               // may not work in extreme cases
   
    string fileList=""
    string files=""
    string pathName = "tmpPath"
    string folders =path+";"                                                    // Remember the full path of all folders in "path" & search each for "ext" files
    string fldr
    do
        fldr = stringFromList(0,folders)
        NewPath/O/Q $pathName, fldr                                             // sets S_path=$path, and creates the symbolic path needed for indexedFile()
        PathInfo $pathName
        files = indexedFile($pathName,-1,ext)                                   // get file names
        if (strlen(files))
            files = fldr+":"+ replaceString(";", removeEnding(files), ";"+fldr+":") // add the full path (folders 'fldr') to every file in the list
            fileList = addListItem(files,fileList)
        endif
        if (recurse)
            folders += indexedDir($pathName,-1,1)                               // get full folder paths
        endif
        folders = removeFromList(fldr, folders)                                 // Remove the folder we just looked at
    while (strlen(folders))
    KillPath $pathName
    return fileList
End

static function /s sanitizeFilePath(path)
    // Avoid annoyances with escape characters when using Microsoft Windows directories.
   
    string path
    path = replaceString("\t", path, "\\t")
    path = replaceString("\r", path, "\\r")
    path = replaceString("\n", path, "\\n")

    return path
end
